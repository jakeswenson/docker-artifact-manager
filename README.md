# docker-artifact-manager
Artifactory front api for manager docker image artifacts

# Building

The project uses maven to build (see build phases below)
```
mvn compile
```

## Result Directory
Builds are built into the `<module>/target` folder

## Build Phases

- `validate`: Runs maven checkstyle
- `compile`: Builds the code
- `test`: Runs the tests
- `package`: Builds a docker container of the api, and a jar for all modules
- `verify`: Runs the code coverage checks
- `deploy`: Pushes the artifacts to the server
- `site`: builds the web-site for the project (including reports like checkstyle and coverage)
- `clean`: cleans the build targets folders

## Maven Build Options

- `-Ddocker.skipBuild`: Skips building a docker container. By default a docker container will be built during the `package` phase
- `-DskipTests`: Skips running tests
- `-Dcobertura.skip`: Skips code coverage checks
- `-Dcheckstyle.skip`: Skips checkstyle

## Maven Check Style Reference
Our checkstyle configuration file is [here](checkstyle.xml)

You can find more info about the modules defined in it here: http://checkstyle.sourceforge.net/checks.html

## API Site
After building using `mvn site` you can find the site for the api in `api/target/site/index.html`

## Running `findbugs`
You can run `findbugs` manually using
```
mvn findbugs:check
```

This will run `findbugs` and fail the build if bugs are found.

### Fixing/Diagnosing `findbugs` issues

`findbugs` includes a GUI to help you fix issues that it finds, by running:
```
mvn findbugs:gui
```

# Artifactory settings

The [settings.xml](settings.xml) file included points at our custom artifactory repos.
Inorder to use these settings you'll want to configure maven to use this settings file.
This is done with the `-s` argument, like `-s settings.xml`

## Deploying artifacts

Should you ever need to deploy artifacts make sure that your
`ARTIFACTORY_USER` and `ARTIFACTORY_PASSWORD` environment variables are set


# Running the api

```
./scripts/run-api.sh
```


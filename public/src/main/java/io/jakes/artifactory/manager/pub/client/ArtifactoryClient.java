package io.jakes.artifactory.manager.pub.client;

import io.jakes.artifactory.manager.pub.Mappers;
import com.google.common.collect.ImmutableList;
import lombok.Value;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.io.IOException;
import java.time.ZonedDateTime;

public interface ArtifactoryClient {
    static ArtifactoryClient createClient(final String baseUri, final String key) {
        final OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new Interceptor() {
                @Override
                public Response intercept(final Chain chain) throws IOException {
                    final Request.Builder builder = chain.request().newBuilder();
                    builder.addHeader("X-JFrog-Art-Api", key);
                    return chain.proceed(builder.build());
                }
            }).build();

        Retrofit retrofit =
            new Retrofit.Builder()
                .baseUrl(baseUri)
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create(Mappers.json()))
                .build();

        ArtifactoryClient service = retrofit.create(ArtifactoryClient.class);

        return service;
    }

    @GET("api/repositories?type=local")
    Call<ImmutableList<Repository>> listRepositories();

    @GET("api/docker/{repoKey}/v2/{dockerRepo}/tags/list")
    Call<DockerTags> listDockerTags(
        @Path("repoKey") final String repoKey,
        @Path("dockerRepo") final String dockerRepo);

    @GET("api/docker/{repoKey}/v2/_catalog")
    Call<DockerRepositories> listDockerRepos(@Path("repoKey") final String repoKey);

    @GET("api/docker/{repoKey}/v2/{dockerRepo}/manifests/{tag}")
    Call<DockerManifest> getDockerManifest(
        @Path("repoKey") final String repoKey,
        @Path("dockerRepo") final String dockerRepo,
        @Path("tag") final String tag);

    @GET("api/storage/{repoKey}/{filePath}")
    Call<ArtifactFile> getFile(
        @Path("repoKey") final String repoKey,
        @Path(value = "filePath", encoded = true) final String filePath);

    @GET("api/storage/{repoKey}/?list&deep=1")
    Call<ArtifactFolder> listFiles(
        @Path("repoKey") final String repoKey);

    @GET("api/storage/{repoKey}/{folderPath}?list&deep=1")
    Call<ArtifactFolder> listFiles(
        @Path("repoKey") final String repoKey,
        @Path(value = "folderPath", encoded = true) final String folderPath);


    @DELETE("api/docker/{repoKey}/v2/{dockerRepo}/manifests/{digest}")
    Call<ResponseBody> deleteDockerByDigest(
        @Path("repoKey") final String repoKey,
        @Path("dockerRepo") final String dockerRepo,
        @Path("digest") final String digest);

    @Value
    class DockerTags {
        private final String name;
        private final ImmutableList<String> tags;
    }

    @Value
    class DockerRepositories {
        private final ImmutableList<String> repositories;
    }

    @Value
    class Repository {
        private final String key;
        private final String type;
        private final String description;
        private final String url;
    }

    @Value
    class DockerManifest {
        private final String name;
        private final String tag;
        private final ImmutableList<DockerLayer> fsLayers;

        private final ImmutableList<Object> history;
        private final String signature;
    }

    @Value
    class DockerLayer {
        private final String blobSum;
    }

    @Value
    class ArtifactFile {
        private final String uri;
        private final String downloadUri;
        private final String repo;
        private final String path;
        private final String remoteUrl;
        private final ZonedDateTime created;
        private final String createdBy;
        private final ZonedDateTime lastModified;
        private final String modifiedBy;
        private final ZonedDateTime lastUpdated;

        private final long size;
        private final String mimeType;
    }

    @Value
    class ArtifactFolder {
        private final String uri;
        private final ZonedDateTime created;

        private final ImmutableList<ArtifactFile> files;

    }
}

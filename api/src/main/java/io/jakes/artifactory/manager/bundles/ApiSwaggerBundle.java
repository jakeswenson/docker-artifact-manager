package io.jakes.artifactory.manager.bundles;

import io.jakes.artifactory.manager.ServiceApplication;
import io.jakes.artifactory.manager.ServiceConfiguration;
import io.jakes.artifactory.manager.swagger.ValueModelResolver;
import io.dropwizard.ConfiguredBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.paradoxical.dropwizard.swagger.SwaggerAssetsBundle;
import io.swagger.jaxrs.config.BeanConfig;

public class ApiSwaggerBundle implements ConfiguredBundle<ServiceConfiguration> {
    @Override
    public void run(final ServiceConfiguration configuration, final Environment environment) throws Exception {

    }

    @Override
    public void initialize(final Bootstrap<?> bootstrap) {
        ValueModelResolver.register();
        bootstrap.addBundle(new SwaggerAssetsBundle(this::getPublicSwagger));
    }

    private BeanConfig getPublicSwagger(final Environment environment) {

        final BeanConfig swagConfig = new BeanConfig();

        swagConfig.setTitle("Artifactory manager API");
        swagConfig.setDescription("Artifactory manager API");
        swagConfig.setLicense("Apache 2.0");
        swagConfig.setResourcePackage(ServiceApplication.class.getPackage().getName());
        swagConfig.setLicenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html");
        swagConfig.setContact("jake@jakeswenson.com");

        swagConfig.setBasePath(environment.getApplicationContext().getContextPath());

        swagConfig.setVersion("1.0");

        swagConfig.setScan(true);

        return swagConfig;
    }
}

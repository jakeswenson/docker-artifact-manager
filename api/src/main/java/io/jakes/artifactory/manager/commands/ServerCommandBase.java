package io.jakes.artifactory.manager.commands;

import com.godaddy.logging.Logger;
import io.dropwizard.cli.EnvironmentCommand;
import io.dropwizard.setup.Environment;
import io.jakes.artifactory.manager.EnvironmentVariables;
import io.jakes.artifactory.manager.ServiceApplication;
import io.jakes.artifactory.manager.ServiceConfiguration;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.eclipse.jetty.util.component.LifeCycle;

import javax.annotation.Nonnull;

import static com.godaddy.logging.LoggerFactory.getLogger;

public abstract class ServerCommandBase extends EnvironmentCommand<ServiceConfiguration> {
    private static final Logger logger = getLogger(ServerCommandBase.class);

    private static final String DefaultConfigFile = getDefaultConfigFile();

    @Nonnull
    private static String getDefaultConfigFile() {

        final String appRoot = EnvironmentVariables.Current.getAppRoot().orElse("/data");

        return String.format("%s/conf/configuration.yml", appRoot);
    }

    protected ServerCommandBase(final String name, final String description, final ServiceApplication application) {
        super(application, name, description);
    }

    @Override
    protected Class<ServiceConfiguration> getConfigurationClass() {
        return ServiceConfiguration.class;
    }

    @Override
    public void configure(final Subparser subparser) {
        subparser.addArgument("file")
                 .nargs("?")
                 .setDefault(DefaultConfigFile)
                 .help("application configuration file");
    }

    @Override
    protected void run(final Environment env, final Namespace namespace, final ServiceConfiguration config) throws Exception {
        startServer(config, env);
    }

    private void startServer(final ServiceConfiguration configuration, final Environment environment) throws Exception {
        final Server server = configuration.getServerFactory().build(environment);
        try {
            server.addLifeCycleListener(new LifeCycleListener());
            cleanupAsynchronously();
            server.start();
        }
        catch (Exception e) {
            logger.error("Unable to start server, shutting down", e);
            server.stop();
            cleanup();
            throw e;
        }
    }

    private class LifeCycleListener extends AbstractLifeCycle.AbstractLifeCycleListener {
        @Override
        public void lifeCycleStopped(final LifeCycle event) {
            cleanup();
        }
    }
}

package io.jakes.artifactory.manager.modules;

import io.jakes.artifactory.manager.modules.common.JsonMapperModule;
import io.jakes.artifactory.manager.modules.artifactory.ArtifactoryModule;
import com.google.inject.Module;

import java.util.Arrays;
import java.util.List;

public final class DefaultApiModules {
    private DefaultApiModules() {
    }

    public static List<Module> modules() {
        return Arrays.asList(
                new JsonMapperModule(),
                new ArtifactoryModule());
    }
}

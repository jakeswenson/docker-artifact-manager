package io.jakes.artifactory.manager.modules.common;

import io.jakes.artifactory.manager.serialization.JacksonJsonMapper;
import io.jakes.artifactory.manager.serialization.JsonMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class JsonMapperModule extends AbstractModule {

    @Override protected void configure() {
    }

    @Provides
    @Singleton
    public JsonMapper getJsonMapper() {
        return new JacksonJsonMapper();
    }
}

package io.jakes.artifactory.manager.commands;

import io.jakes.artifactory.manager.ServiceApplication;
import io.jakes.artifactory.manager.ServiceConfiguration;
import io.jakes.artifactory.manager.bundles.ApiGuiceBundleProvider;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.inf.Namespace;

public class ApiServerCommand extends ServerCommandBase {
    public ApiServerCommand(final ServiceApplication application) {
        super("api", "Starts the application up as an api node", application);
    }

    private void initialize(final Bootstrap<ServiceConfiguration> bootstrap) {
        bootstrap.addBundle(new ApiGuiceBundleProvider().getBundle());
    }

    @Override
    public void run(final Bootstrap<?> wildcardBootstrap, final Namespace namespace) throws Exception {
        initialize((Bootstrap<ServiceConfiguration>) wildcardBootstrap);
        super.run(wildcardBootstrap, namespace);
    }
}

package io.jakes.artifactory.manager.bundles;

import io.jakes.artifactory.manager.modules.DefaultApiModules;
import com.google.inject.Module;

import java.util.List;

public class ApiGuiceBundleProvider extends BaseGuiceBundleProvider {
    @Override
    protected List<Module> getModules() {
        return DefaultApiModules.modules();
    }
}


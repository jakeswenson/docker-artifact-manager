package io.jakes.artifactory.manager

import io.jakes.artifactory.manager.pub.client.ArtifactoryClient

interface ArtifactoryClientFactory {
    fun createClient(apiKey: String): ArtifactoryClient
}

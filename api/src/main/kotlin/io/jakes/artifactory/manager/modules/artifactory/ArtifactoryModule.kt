package io.jakes.artifactory.manager.modules.artifactory

import com.google.common.base.Strings
import com.google.inject.AbstractModule
import com.google.inject.Provides
import io.jakes.artifactory.manager.ArtifactoryClientFactory
import io.jakes.artifactory.manager.ServiceConfiguration
import io.jakes.artifactory.manager.pub.client.ArtifactoryClient

class ArtifactoryModule: AbstractModule() {
    override fun configure() {
    }

    @Provides
    fun clientFactory(configuration: ServiceConfiguration): ArtifactoryClientFactory {
        val artifactory = configuration.artifactory
        val serverUrl = artifactory.url

        return object : ArtifactoryClientFactory {
            override fun createClient(apiKey: String): ArtifactoryClient {

                if (Strings.isNullOrEmpty(apiKey)) {
                    return ArtifactoryClient.createClient(serverUrl, artifactory.apiKey)
                }

                val keyPrefix = "Key ";

                return ArtifactoryClient.createClient(serverUrl, apiKey.substring(keyPrefix.length))
            }
        }
    }
}

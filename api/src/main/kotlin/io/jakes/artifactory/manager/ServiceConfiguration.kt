package io.jakes.artifactory.manager

import io.jakes.artifactory.manager.configurations.ArtifactoryConfiguration
import io.dropwizard.Configuration

class ServiceConfiguration : Configuration() {
    var artifactory = ArtifactoryConfiguration()
}
